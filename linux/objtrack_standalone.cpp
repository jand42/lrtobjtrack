#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <opencv/cv.h>
#include <opencv/highgui.h>

// Shared memory size: 2x 16bit signed int
#define SHMSZ 2

int main(int argc, char *argv[])
{
  struct termios options;
  int uart;
  int wnb;
  uint8_t txbuf[5];
  
  uart = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
  if (uart == -1) {
    perror("open_port: Unable to open /dev/ttyUSB0");
  } else {
    fcntl(uart, F_SETFL, 0);
    tcgetattr(uart, &options);
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);
    options.c_cflag |= (CLOCAL | CREAD);
    tcsetattr(uart, TCSANOW, &options);
  }
  
  int verbose = 0, preset = 0;
  int x = -1, y = -1;
  static const int ballArea = 300000; // Approximate ball area (compare it with objArea)

  double objArea;
  cv::Moments objMoments; // Moments of thresholded picture
  double objMoment01; // Responsible for Y
  double objMoment10; // Responsible for X

  cv::VideoCapture cam(0);

  cv::Mat img;
  cv::Mat imgHSV;
  cv::Mat imgThresholded;

  int h1 = 0, h2 = 0, s1 = 0, s2 = 0, v1 = 0, v2 = 0;

  if (argc == 1) {
    fprintf(stdout, "Running in normal mode");
  }
  else {
    fprintf(stdout, "Running in verbose config mode");
    verbose = 1;
  }

  if (verbose) {
    cvNamedWindow("Original Image", CV_WINDOW_AUTOSIZE);
    cvNamedWindow("Thresholded Image", CV_WINDOW_AUTOSIZE);
    cvNamedWindow("Config", CV_WINDOW_AUTOSIZE);

    cvCreateTrackbar("H1","Config",&h1,255,0);
    cvCreateTrackbar("H2","Config",&h2,255,0);
    cvCreateTrackbar("S1","Config",&s1,255,0);
    cvCreateTrackbar("S2","Config",&s2,255,0);
    cvCreateTrackbar("V1","Config",&v1,255,0);
    cvCreateTrackbar("V2","Config",&v2,255,0);
  }
  else {
    // red ball
    // h1 = 0;
    // h2 = 180;
    // s1 = 94;
    // s2 = 178;
    // v1 = 0;
    // v2 = 255;
    // orange ball
    h1 = 0;
    h2 = 123;
    s1 = 127;
    s2 = 201;
    v1 = 0;
    v2 = 255;
  }

  while(1) {
    cam.read(img);

    cv::cvtColor(img, imgHSV, cv::COLOR_BGR2HSV);
    cv::inRange(imgHSV, 
		cv::Scalar(h1, s1, v1),
		cv::Scalar(h2, s2, v2),
		imgThresholded);

    // Morphological opening (removes small objects from the foreground)
    cv::erode (imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
    cv::dilate(imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));


    // Morphological closing (removes small holes from the foreground)
    cv::dilate(imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
    cv::erode(imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));

    // Calculating the moments of the thresholded image
    objMoments = cv::moments(imgThresholded);

    objMoment01 = objMoments.m01;
    objMoment10 = objMoments.m10;
    objArea = objMoments.m00;

    std::cout << "Object area: " << objArea << std::endl;
    //if (objArea > ballArea) {
    if (1) {
      x = objMoment10 / objArea;
      y = objMoment01 / objArea;
      cv::circle(img, cv::Point(x, y), 10, cv::Scalar(0, 0, 255), -1);
      std::cout << "(" << x << "," << y << ")" << std::endl;
    }

    if (verbose) {
      cv::imshow("Original Image", img);
      cv::imshow("Thresholded Image", imgThresholded);
      if (cv::waitKey(10) == 1048603) {
	break;
      }
    }

    // Write ball position to UART
    uint16_t xs = static_cast<uint16_t>(x);
    uint16_t ys = static_cast<uint16_t>(y);
    txbuf[0] = static_cast<uint8_t>('$');
    txbuf[1] = x >> 8;
    txbuf[2] = x & 0x00FF;
    txbuf[3] = y >> 8;
    txbuf[4] = y & 0x00FF;
    wnb = write(uart, txbuf, 5);
    if (wnb < 0) {
      fputs("write() failed!\n", stderr);
      return 0;
    }

    // 30 FPS
    // usleep(1/30000);
  }

  cvDestroyAllWindows();
  return 0;
}
